Source: sop-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Jérôme Charaoui <jerome@riseup.net>,
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 default-jdk-doc,
 gradle-debian-helper,
 javahelper,
 junit5 <!nocheck>,
 libpicocli-java (>> 4),
 maven-repo-helper,
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/java-team/sop-java.git
Vcs-Browser: https://salsa.debian.org/java-team/sop-java
Homepage: https://github.com/pgpainless/sop-java

Package: libsop-java-java
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Stateless OpenPGP Protocol API and CLI for Java
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains definitions for a set of Java interfaces describing
 the Stateless OpenPGP Protocol.

Package: libsop-java-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 libjs-jquery,
 libjs-jquery-ui,
 ${misc:Depends},
Description: Stateless OpenPGP Protocol API and CLI for Java - docs
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains documentation for libsop-java-java.

Package: libsop-java-picocli-java
Architecture: all
Depends:
 libpicocli-java (>> 4),
 libsop-java-java,
 ${misc:Depends},
Description: Stateless OpenPGP Protocol API and CLI for Java - picocli wrapper
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains a wrapper application that transforms the sop-java API
 into a command line application compatible with the SOP-CLI specification.

Package: libsop-java-picocli-java-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 libjs-jquery,
 libjs-jquery-ui,
 ${misc:Depends},
Description: Stateless OpenPGP Protocol API and CLI for Java - picocli wrapper docs
 The Stateless OpenPGP Protocol specification defines a generic stateless
 CLI for dealing with OpenPGP messages. Its goal is to provide a minimal,
 yet powerful API for the most common OpenPGP related operations.
 .
 This package contains documentation for libsop-java-picocli-java.
