# SPDX-FileCopyrightText: 2022 Paul Schaub <vanitasvitae@fsfe.org>
#
# SPDX-License-Identifier: Apache-2.0
sop.name=sop
usage.header=Stateless OpenPGP Protocol
locale=Locale for description texts

# Generic
usage.synopsisHeading=Usage:\u0020
usage.commandListHeading = %nCommands:%n
usage.optionListHeading = %nOptions:%n
usage.footerHeading=Powered by picocli%n

# Exit Codes
usage.exitCodeListHeading=%nExit Codes:%n
usage.exitCodeList.0=\u00200:Successful program execution
usage.exitCodeList.1=\u00201:Generic program error
usage.exitCodeList.2=\u00203:Verification requested but no verifiable signature found
usage.exitCodeList.3=13:Unsupported asymmetric algorithm
usage.exitCodeList.4=17:Certificate is not encryption capable
usage.exitCodeList.5=19:Usage error: Missing argument
usage.exitCodeList.6=23:Incomplete verification instructions
usage.exitCodeList.7=29:Unable to decrypt
usage.exitCodeList.8=31:Password is not human-readable
usage.exitCodeList.9=37:Unsupported Option
usage.exitCodeList.10=41:Invalid data or data of wrong type encountered
usage.exitCodeList.11=53:Non-text input received where text was expected
usage.exitCodeList.12=59:Output file already exists
usage.exitCodeList.13=61:Input file does not exist
usage.exitCodeList.14=67:Cannot unlock password protected secret key
usage.exitCodeList.15=69:Unsupported subcommand
usage.exitCodeList.16=71:Unsupported special prefix (e.g. \"@env/@fd\") of indirect parameter
usage.exitCodeList.17=73:Ambiguous input (a filename matching the designator already exists)
usage.exitCodeList.18=79:Key is not signing capable

## SHARED RESOURCES
## Malformed Input
sop.error.input.malformed_session_key=Session keys are expected in the format 'ALGONUM:HEXKEY'.
sop.error.input.not_a_private_key=Input '%s' does not contain an OpenPGP private key.
sop.error.input.not_a_certificate=Input '%s' does not contain an OpenPGP certificate.
sop.error.input.not_a_signature=Input '%s' does not contain an OpenPGP signature.
sop.error.input.malformed_not_after=Invalid date string supplied as value of '--not-after'.
sop.error.input.malformed_not_before=Invalid date string supplied as value of '--not-before'.
sop.error.input.stdin_not_a_message=Standard Input appears not to contain a valid OpenPGP message.
sop.error.input.stdin_not_a_private_key=Standard Input appears not to contain a valid OpenPGP secret key.
sop.error.input.stdin_not_openpgp_data=Standard Input appears not to contain valid OpenPGP data
## Indirect Data Types
sop.error.indirect_data_type.ambiguous_filename=File name '%s' is ambiguous. File with the same name exists on the filesystem.
sop.error.indirect_data_type.environment_variable_not_set=Environment variable '%s' not set.
sop.error.indirect_data_type.environment_variable_empty=Environment variable '%s' is empty.
sop.error.indirect_data_type.input_file_does_not_exist=Input file '%s' does not exist.
sop.error.indirect_data_type.input_not_a_file=Input file '%s' is not a file.
sop.error.indirect_data_type.output_file_already_exists=Output file '%s' already exists.
sop.error.indirect_data_type.output_file_cannot_be_created=Output file '%s' cannot be created.
sop.error.indirect_data_type.illegal_use_of_env_designator=Special designator '@ENV:' cannot be used for output.
sop.error.indirect_data_type.designator_env_not_supported=Special designator '@ENV' is not supported.
sop.error.indirect_data_type.designator_fd_not_supported=Special designator '@FD' is not supported.
## Runtime Errors
sop.error.runtime.no_backend_set=No SOP backend set.
sop.error.runtime.cannot_unlock_key=Cannot unlock password-protected secret key from input '%s'.
sop.error.runtime.key_uses_unsupported_asymmetric_algorithm=Secret key from input '%s' uses an unsupported asymmetric algorithm.
sop.error.runtime.cert_uses_unsupported_asymmetric_algorithm=Certificate from input '%s' uses an unsupported asymmetric algorithm.
sop.error.runtime.key_cannot_sign=Secret key from input '%s' cannot sign.
sop.error.runtime.cert_cannot_encrypt=Certificate from input '%s' cannot encrypt.
sop.error.runtime.no_session_key_extracted=Session key not extracted. Feature potentially not supported.
sop.error.runtime.no_verifiable_signature_found=No verifiable signature found.
## Usage errors
sop.error.usage.password_or_cert_required=At least one password file or cert file required for encryption.
sop.error.usage.argument_required=Argument '%s' is required.
sop.error.usage.parameter_required=Parameter '%s' is required.
sop.error.usage.option_requires_other_option=Option '%s' is requested, but no option %s was provided.
# Feature Support
sop.error.feature_support.subcommand_not_supported=Subcommand '%s' is not supported.
sop.error.feature_support.option_not_supported=Option '%s' not supported.